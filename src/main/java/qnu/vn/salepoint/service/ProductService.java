package qnu.vn.salepoint.service;

import java.util.List;
import java.util.Optional;

import org.salespointframework.catalog.ProductIdentifier;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import qnu.vn.salepoint.entity.ProductEntity;
import qnu.vn.salepoint.entity.ProductEntity.ProductType;

@Service
public interface ProductService {
	
	
	void CreateProduct(ProductEntity productEntity);

	void UpdateProduct(ProductEntity productEntity);

	void DeleteProduct(ProductIdentifier id);

	@Query("SELECT * FROM product")
	List<ProductEntity> getAllProduct();
	
	List<ProductEntity> getAllByProductType(ProductType productType);
	
	Optional<ProductEntity> getProductByProductId(ProductIdentifier id);
}
