package qnu.vn.salepoint.service.impl;

import java.util.List;
import java.util.Optional;

import org.salespointframework.catalog.ProductIdentifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import qnu.vn.salepoint.entity.ProductEntity;
import qnu.vn.salepoint.entity.ProductEntity.ProductType;
import qnu.vn.salepoint.repository.ProductRepository;
import qnu.vn.salepoint.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepository productRepository;

	@Override
	public void CreateProduct(ProductEntity productEntity) {
		productRepository.save(productEntity);
	}

	@Override
	public void UpdateProduct(ProductEntity productEntity) {

	}

	@Override
	public List<ProductEntity> getAllProduct() {
		return (List<ProductEntity>) productRepository.findAll();
	}

	@Override
	public void DeleteProduct(ProductIdentifier id) {
		productRepository.deleteById(id);
	}

	@Override
	public List<ProductEntity> getAllByProductType(ProductType productType) {

		return productRepository.findByType(productType, ProductRepository.DEFAULT_SORT);
	}

	@Override
	public Optional<ProductEntity> getProductByProductId(ProductIdentifier id) {
		return productRepository.findById(id);
	}
}
