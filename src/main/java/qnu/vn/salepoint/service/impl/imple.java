package qnu.vn.salepoint.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.salespointframework.catalog.ProductIdentifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.util.Streamable;

import qnu.vn.salepoint.entity.ProductEntity;
import qnu.vn.salepoint.entity.ProductEntity.ProductType;
import qnu.vn.salepoint.repository.ProductRepository;

public class imple implements ProductRepository{

	@Override
	public Streamable<ProductEntity> findByCategory(String category) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Streamable<ProductEntity> findByAllCategories(Collection<String> categories) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Streamable<ProductEntity> findByAnyCategory(Collection<String> categories) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Streamable<ProductEntity> findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends ProductEntity> S save(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends ProductEntity> Iterable<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Optional<ProductEntity> findById(ProductIdentifier id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(ProductIdentifier id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Iterable<ProductEntity> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<ProductEntity> findAllById(Iterable<ProductIdentifier> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(ProductIdentifier id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(ProductEntity entity) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends ProductEntity> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<ProductEntity> findByType(ProductType type, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

}
