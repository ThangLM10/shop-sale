package qnu.vn.salepoint.repository;

import java.util.List;

import org.salespointframework.catalog.Catalog;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import qnu.vn.salepoint.entity.ProductEntity;
import qnu.vn.salepoint.entity.ProductEntity.ProductType;

@Repository
public interface ProductRepository extends Catalog<ProductEntity> {
	static final Sort DEFAULT_SORT = Sort.by("productIdentifier").descending();

	List<ProductEntity> findByType(ProductType type, Sort sort);

}
