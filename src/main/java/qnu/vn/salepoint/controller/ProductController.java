package qnu.vn.salepoint.controller;

import org.salespointframework.catalog.ProductIdentifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import qnu.vn.salepoint.entity.ProductEntity.ProductType;
import qnu.vn.salepoint.service.ProductService;

@Controller
//@RequestMapping(path = "/")
public class ProductController {
	@Autowired
	ProductService productService;
	
	@RequestMapping(value = { "/", "/index" })
	String getAllProduct (Model model) {
		model.addAttribute("productList", productService.getAllProduct());
		System.out.println(productService.getAllProduct());
		return "index";
	}
	
	@RequestMapping(value = "/product-content")
	String getAllProductContent (Model model) {
		model.addAttribute("productList", productService.getAllProduct());
		System.out.println(productService.getAllProduct());
		return "/product/product";
	}
	
	@GetMapping("/product/product-detail/{product-id}")
	String productDetail(@PathVariable("product-id") ProductIdentifier productId, Model model) {
		model.addAttribute("detail", productService.getProductByProductId(productId).get());
		return "detail";
	}
	
	@GetMapping("/men-catalog")
	String menCatalog (Model model) {
		model.addAttribute("menCatalog", productService.getAllByProductType(ProductType.MEN));
		System.out.println(productService.getAllByProductType(ProductType.MEN));
		return "shop";
	}
	
	@GetMapping("/women-catalog")
	String womenCatalog (Model model) {
		model.addAttribute("womenCatalog", productService.getAllByProductType(ProductType.WOMEN));
		System.out.println(productService.getAllByProductType(ProductType.WOMEN));
		return "index";
	}
	
	@GetMapping("/shoes-catalog")
	String shoesCatalog (Model model) {
		model.addAttribute("showCatalog", productService.getAllByProductType(ProductType.SHOES));
		System.out.println(productService.getAllByProductType(ProductType.SHOES));
		return "index";
	}
}
