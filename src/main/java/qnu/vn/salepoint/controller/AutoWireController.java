package qnu.vn.salepoint.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AutoWireController {
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String hello(HttpServletRequest request) throws Exception {
		return "index";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpServletRequest request) throws Exception {
		return "login";
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register(HttpServletRequest request) throws Exception {
		return "register";
	}
	
	@RequestMapping(value = "/cart", method = RequestMethod.GET)
	public String cart(HttpServletRequest request) throws Exception {
		return "cart";
	}
	
	@RequestMapping(value = "/home-02", method = RequestMethod.GET)
	public String home2(HttpServletRequest request) throws Exception {
		return "home-02";
	}
	
	@RequestMapping(value = "/home-03", method = RequestMethod.GET)
	public String home3(HttpServletRequest request) throws Exception {
		return "home-03";
	}
	
	@RequestMapping(value = "/contact", method = RequestMethod.GET)
	public String contact(HttpServletRequest request) throws Exception {
		return "contact";
	}
	
	@RequestMapping(value = "/product", method = RequestMethod.GET)
	public String product(HttpServletRequest request) throws Exception {
		return "product/product";
	}
	
	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String productDetail(HttpServletRequest request) throws Exception {
		return "detail";
	}
	
	@RequestMapping(value = "/blog", method = RequestMethod.GET)
	public String blog(HttpServletRequest request) throws Exception {
		return "blog";
	}
	
	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public String about(HttpServletRequest request) throws Exception {
		return "about";
	}
	
	@RequestMapping(value = "/blog-detail", method = RequestMethod.GET)
	public String blogDetail(HttpServletRequest request) throws Exception {
		return "blog-detail";
	}
	
	@RequestMapping(value = "/main-header", method = RequestMethod.GET)
	public String header(HttpServletRequest request) throws Exception {
		return "fragments/main-header";
	}
	
}
